import React from "react";
import Atrasbtn from "./Atrasbtn.jsx";
import Menucola from "./Menucola.jsx";
import Tablacola from "./Tablacola.jsx";
import Menuticketcl from "./Menuticketcl.jsx";
import { connect } from 'react-redux';

const URL = "http://localhost:3001/codigo";

class Crearticketcl extends React.Component {

  constructor(props){
    super (props)

    this.updateLista = this.updateLista.bind(this);
  }
  componentDidMount() {
    this.updateLista();
  }
  updateLista() {

    // Fetch GET a la base de datos para actualizar los datos de la lista Redux 
    fetch(URL)
      .then(datos => datos.json())
      .then(codigos => this.props.dispatch({
        type: 'NOU_ELEMENT',
        codigos: codigos,
      }))
      .catch(err => console.log(err));
  }

  render() {

    return (
      <>
        <Atrasbtn />
        <Menuticketcl updateLista={this.updateLista}/>
      </>
    );
  }
}
export default connect()(Crearticketcl);
