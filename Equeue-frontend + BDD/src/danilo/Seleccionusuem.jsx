import React, { Component } from 'react'
import "/src/css/Seleccionusuem.css";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert, Form, FormGroup, Label, Input, FormText, Container, Row, Col } from 'reactstrap';


export default class Seleccionusuem extends Component {
    render() {
        return (
            <>

                <Container className="botones">
                    <Row>
                        <Col xs="2">
                            <Link to="/login" className="btn btn-primary">Cliente</Link>

                        </Col>
                        <Col xs="2">
                            <Link to="/loginemp" className="btn btn-primary">Empresa</Link>
                        </Col>
                    </Row>
                </Container>

            </>
        )
    }
}
