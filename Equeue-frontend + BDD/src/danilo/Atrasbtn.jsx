import React from "react";
import "/src/css/Atrasbtn.css";
import { Button, Container, Row, Col } from 'reactstrap';
import { BrowserRouter, Link, NavLink, Switch, Route, Redirect } from "react-router-dom";




export default class Atrasbtn extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Container>
                <Row>
                    <Col xs="6">

                        {/* Boton "atras" para volver a la lista de "listas" */}
                        <div className="btnatras">
                            <Link to={"/paginaemp"} className="btn btn-secondary linkatras">Atras</Link>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}