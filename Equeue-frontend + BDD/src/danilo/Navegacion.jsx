import React, { Component } from 'react'
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import Opcioneslista from "/src/danilo/opcioneslista.jsx";
import PaginaEmpresa from "/src/marianna/PaginaEmpresa";
import PaginaTurno from "/src/cosmin/paginaturno";
import RegistroUsu from "/src/Facundo/RegistroUsu.js";
import RegistroEm from "/src/Facundo/RegistroEm.js";
import Login from "/src/Facundo/Login.js";
import LoginEm from "/src/Facundo/LoginEm.js";
import Crearticketcl from "/src/danilo/Crearticketcl.jsx";
import "/src/css/navegacion.css";
import { connect } from 'react-redux';
import IntroCodigo from "/src/Facundo/introCodigo.js";
import Inicio from "/src/Facundo/Inicio.js";
import withAuth from "/src/danilo/withAuth.jsx";
import Seleccionusuem from "/src/danilo/Seleccionusuem.jsx";
import Principal from "/src/Facundo/principal.js";



class Navegacion extends Component {


    render() {

        // if(!this.props.email){
        //     return <h1>Cargando...</h1>
        // }

        return (

            // Navbar principal de la aplicacion con enrutamiento.
            <>
                <div className="container-fullwidth">
                    <div className="row">
                        <div className="col-12">
                            <BrowserRouter>
                                <nav className="navbar navbar-expand-sm navbar-dark bg-dark" >
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul className="navbar-nav mr-auto">
                                            <li className="nav-item active">
                                                <Link to="/" className="nav-link active">Inicio</Link>
                                            </li>
                                            {this.props.email ?
                                                <li className="nav-item active">
                                                    <Link to="/introCodigo" className="nav-link active"> Ticket</Link>
                                                </li>
                                                :
                                                <div />
                                            }
                                            {this.props.email ?

                                                <li className="nav-item active">
                                                    <Link to="/paginaemp" className="nav-link active">Colas</Link>
                                                </li>
                                                :
                                                <div />
                                            }
                                        </ul>
                                        {this.props.email ?
                                            <span className="nav-link emaillogin">{this.props.email}</span>
                                            :
                                            <Link to="/inicio" className="btn btn-outline-primary">Iniciar Sesión</Link>
                                        }
                                    </div>
                                </nav>
                                <Switch>
                                    {/* <PaginaPrincipal/> */}
                                    <Route exact path="/" component={Principal} />

                                    {/* Registro/Login */}
                                    <Route path="/inicio" component={Inicio} />
                                    <Route path="/registeruser" component={RegistroUsu} />
                                    <Route path="/registeremp" component={RegistroEm} />
                                    <Route path="/login" component={Login} />
                                    <Route path="/loginemp" component={LoginEm} />
                                    <Route path="/introCodigo" component={IntroCodigo} />
                                    <Route path="/seleccionusuem" component={Seleccionusuem} />

                                    {/* Paginas Autorizadas */}
                                    <Route path="/paginaemp" component={PaginaEmpresa} />
                                    <Route path="/opcioneslista" component={Opcioneslista} />
                                    <Route path="/crearticketcl" component={Crearticketcl} />
                                    <Route path="/turno" component={PaginaTurno} />
                                    <Route path="/introCodigo" component={IntroCodigo} />
                                </Switch>
                            </BrowserRouter>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        email: state.email
    }
}

export default connect(mapStateToProps)(Navegacion);
