import React from "react";
import "/src/css/Menucola.css";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert, Form, FormGroup, Label, Input, FormText, Container, Row, Col } from 'reactstrap';
import {connect} from 'react-redux';
var uniqid = require('uniqid');


const URL = "http://localhost:3001/codigo";

 class Menucola extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalcrear: false,
            modalpausar: false,
            codigounico: "",
            idcliente: "",
            idList: ""
        };

        this.togglecrear = this.togglecrear.bind(this);
        this.togglepausar = this.togglepausar.bind(this);
        this.generarcodigo = this.generarcodigo.bind(this);
        this.submit = this.submit.bind(this);
    }

    submit(e) {

        e.preventDefault();
        //VALIDACION...

        let codigo = {
            code: this.state.codigounico,
            idList: this.props.idlista
        }


        // Fetch POST para introducir los datos en la base de datos
        fetch(URL, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(codigo)
        })
            .then(respuesta => respuesta.json())
            // Ejecuta el metodo updateLista para volver a cargar la lista Redux con los datos introducidos
            .then(this.props.updateLista)
            .catch(err => console.log(err))

    }

    //Generar codigo unico para el ticket
    generarcodigo() {

        this.setState({
            codigounico: uniqid.process()
        })
    }

    // Toggle del Modal
    togglecrear() {

        if (!this.state.modalcrear) {
            this.generarcodigo();
        }
        this.setState(prevState => ({
            modalcrear: !prevState.modalcrear
        }));
    }

    // Toggle del Modal
    togglepausar() {
        this.setState(prevState => ({
            modalpausar: !prevState.modalpausar
        }));
    }
    render() {

        if(!this.props.idlista){

            return <h1>Cargando...</h1>
        }

        console.log(this.props.idlista)
        return (
            <Container>
                <Row>
                    <Col xs="6">
                        <div className="colamenu">

                            {/* Botones de control */}
                            <Button className="btnticket" color="primary" onClick={this.togglecrear}>Crear Ticket</Button>
                            {/* <Button className="btnavanzar" classname="btnavanzar" color="primary">Avanzar Cola</Button>
                            <Button className="btnpausar" color="primary" onClick={this.togglepausar}>Pausar Cola</Button> */}

                            {/* Modal crear ticket */}
                            <Modal isOpen={this.state.modalcrear} toggle={this.togglecrear} className={this.props.className}>
                                <ModalHeader toggle={this.togglecrear} charCode="X">Crear nuevo ticket</ModalHeader>
                                <Form onSubmit={this.submit}>
                                    <ModalBody>
                                    <Alert color="primary">
                                        <h1>{this.state.codigounico}</h1>
                                    </Alert>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="success" type="submit" onClick={this.togglecrear}>Crear Ticket</Button>
                                    <Button color="danger" onClick={this.togglecrear}>Cancelar</Button>
                                </ModalFooter>
                                </Form>
                            </Modal>

                            {/* Modal pausar lista */}
                            <Modal isOpen={this.state.modalpausar} toggle={this.togglepausar} className={this.props.className}>
                                <ModalHeader toggle={this.togglepausar} charCode="X">Pausar cola actual</ModalHeader>
                                <ModalBody>
                                    <Form>
                                        <FormGroup>
                                            <Label for="tiempopausa">Tiempo de pausa</Label>
                                            <Input type="text" name="tiempopausa" id="tiempopausa" placeholder="Introduce el tiempo que va a estar pausada la cola" />
                                        </FormGroup>
                                    </Form>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="success" onClick={this.togglepausar}>Pausar Cola</Button>
                                    <Button color="danger" onClick={this.togglepausar}>Cancelar</Button>
                                </ModalFooter>
                            </Modal>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}
function mapStateToProps(state){

    return {
        idlista: state.idlista
    }
}

export default connect(mapStateToProps)(Menucola);