import React from "react";
import Atrasbtn from "./Atrasbtn.jsx";
import Menucola from "./Menucola.jsx";
import Tablacola from "./Tablacola.jsx";
import { connect } from 'react-redux';

const URL = "http://localhost:3001/codigo/";

class Opcioneslista extends React.Component {

  constructor(props) {
    super(props)

    this.updateLista = this.updateLista.bind(this);
  }
  componentDidMount() {
    this.updateLista();
  }
  updateLista() {

    if (!this.props.idlista) {

    } else {

      // Fetch GET a la base de datos para actualizar los datos de la lista Redux 
      fetch(URL + this.props.idlista)
        .then(datos => datos.json())
        .then(codigos => this.props.dispatch({
          type: 'NOU_ELEMENT',
          codigos: codigos,
        }))
        .catch(err => console.log(err));
    }

  }

  render() {

    return (
      <>
        <Atrasbtn />
        <Menucola updateLista={this.updateLista} />
        <br />
        <Tablacola updateLista={this.updateLista} />
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    idlista: state.idlista
  }
}

export default connect(mapStateToProps)(Opcioneslista);
