import React from "react";
import "/src/css/Tablacola.css";
import { Table, Container, Row, Col, Spinner } from 'reactstrap';
import { connect } from 'react-redux';

const URL = "http://localhost:3001/codigo";

class Tablacola extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            titulo: "Cola 1",
            proxcodigo: ""
        };

        this.deleteItem = this.deleteItem.bind(this);
    }

    // Modifica el valor "is_closed" para cerrar ese ticket y que no lo muestre en la lista
    deleteItem(itemId) {

        let cerrado = {
            is_closed: 1
          }
      
          fetch(URL+"/"+itemId, {
            method: 'PUT',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(cerrado)
          })
            .then(respuesta => respuesta.json())
            .then(this.props.updateLista)
            .catch(err => console.log(err))
      

        // if (!itemId) return;
        // fetch(URL + itemId, { method: 'DELETE' })
        //     .then(this.props.updateLista)
        //     .catch(err => console.log(err))
    }
    render() {

        let titulo;
        let filas;
        let spinner = "";
        let i = 1;

        if (!this.props.nombrelista){
            return <h1>Cargando...</h1>
        }
        // Comprobar si la lista esta vacia para mostrar el mensaje "Cargando..."
        if (this.props.lista.length < 1) {
            titulo = "Cargando datos";
            spinner = <Spinner color="primary" />;
        } else {
            // Llenar una array con los datos recuperados de la lista Redux e introducirlos en formato tabla HTML

            filas = this.props.lista.data.map(el =>
                <tr key={el.id}>
                    <td>{i++}</td>
                    <td>{el.code}</td>
                    <td>
                        <i style={{ cursor: "pointer" }} className='fa fa-lg fa-trash text-danger' onClick={() => this.deleteItem(el.id)}></i>
                    </td>
                </tr>)

            titulo = this.props.nombrelista;
        }

        return (
            <Container>
                <Row>
                    <Col xs="12">
                        <h1 className="spinner">{titulo}</h1>
                        <div className="spinner">
                            {spinner}
                        </div>
                        <Table>
                            <thead>
                                <tr>
                                    {/* <th>#</th>
                                    <th>Código</th> */}
                                </tr>
                            </thead>
                            <tbody>
                                {filas}
                            </tbody>

                        </Table>
                    </Col>
                </Row>
            </Container>

        );
    }
}
const mapStateToProps = (state) => {

    return {
        lista: JSON.parse(JSON.stringify(state.codigos)),
        nombrelista: state.nombrelista
    }
    
}

export default connect(mapStateToProps)(Tablacola);

