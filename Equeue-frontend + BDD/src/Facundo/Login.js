import React from "react";
import "/src/css/style.css";
import { Button, Input, Container, Col, Form, FormGroup, Label } from 'reactstrap';
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { connect } from 'react-redux';


const URL = "http://localhost:3001";

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        let usuari = {
            email: this.state.email,
            password: this.state.password
        }
        fetch(URL + '/cliente/login', {
            method: 'POST',
            //important:  credentials: 'include'
            credentials: 'include',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(usuari)
        })
            .then(res => {
                if (res.status === 200) {
                    return res;
                } else {
                    const error = new Error(res.error);
                    throw error;
                }
            })
            .then(res => res.json())
            .then(res => {
                let email = res.email;
                let id = res.id;

                console.log("eureka! " + email);
                this.props.dispatch({
                    type: 'SET_USUARIO',
                    id: id,
                    email: email,
                    admin: false
                });
                this.props.history.push('/');


            })
            .catch(err => console.log(err));

    }
    render() {
        return (
            <div className="content">
                <div className="login">
                    <h2> Inicio de Sesión</h2>
                    <Form className="form" onSubmit={this.handleSubmit}>
                        <Col>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="email"
                                    value={this.state.email} onChange={this.handleInputChange}
                                    name="email"
                                    placeholder="Introduzca su Email"></Input>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Contraseña</Label>
                                <Input type="password"
                                    value={this.state.password} onChange={this.handleInputChange}
                                    name="password"
                                    placeholder="**********"></Input>
                            </FormGroup>
                        </Col>
                        <Button type="submit"
                            color="primary">Iniciar Sesión</Button>

                        <Link to={"/registeruser"} className="btn btn-primary registrate">
                            Regístrate
                        </Link>
                    </Form>
                </div>
            </div>
        )
    }
}
export default connect()(Login);