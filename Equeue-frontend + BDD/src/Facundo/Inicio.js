import React from "react";
import "/src/css/style.css";
import { Link } from 'react-router-dom';

export default class Inicio extends React.Component {
    render(){
        return(
            <div className="content">
                <div className="intro">
                    <div className="introContent">
                            <h2 className="tituloIntro"> eQueue </h2>
                    <Link to={"/login"} className="btn btn-secondary btn-lg inicioButton">
                            Cliente
                    </Link>
                    <Link to={"/loginemp"} className="btn btn-secondary btn-lg inicioButton">
                            Empresa
                    </Link>
                    </div>
                </div>
            </div>
        )
    }
}