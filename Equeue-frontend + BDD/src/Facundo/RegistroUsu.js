import React from "react";
import { Button, Input, Container, Col, Form, FormGroup, Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Redirect } from 'react-router';

const URL = "http://localhost:3001";

export default class Registro extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            email: "",
            password: "",
            password2: "",
            notificacion: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleOptionChange(event) {
        this.setState({
            notificacion: event.target.value
        });
    }
    handleInputChange(event) {

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

    }

    handleSubmit(e) {

        e.preventDefault();


        let usuari = {
            email: this.state.email,
            password: this.state.password,
            //active_notification: this.state.notificacion
        }
        fetch(URL + '/cliente/register', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(usuari)
        })
            .then(respuesta => respuesta.json())
            .then(respuesta => {
                if (respuesta.ok === false) {
                    throw "ERROR REGISTRANT USUARI!";
                } else {
                    console.log("OK USUARI REGISTRAT!!!");
                    this.props.history.push('/login');
                }
            })
            .catch(err => console.log(err));

    }
    render() {
        return (
            <div className="content">
                <div className="login">
                    <h2> Registro de Usuario</h2>
                <Form className="form" onSubmit={this.handleSubmit}>
                    <Col>
                        <FormGroup>
                            <Label>Email (Usuario)</Label>
                            <Input type="email"
                                value={this.state.email} onChange={this.handleInputChange}
                                placeholder="ejemplo@ejemplo.com"
                                // required pattern="[aA-zZ]{5,}"
                                name="email"
                               title="Su email debe tener el formato texto@texto"></Input>
                        </FormGroup>
                    </Col>
                    <Col>
                        {/* <FormGroup>
                            <Label>Nº de Teléfono</Label>
                            <Input type="text"
                                // value={this.state.email} onChange={this.handleInputChange}
                                placeholder="Introduzca su Número"
                                name="telefono"
                                // required
                                // pattern="[0-9]{9}"
                                ></Input>
                        </FormGroup> */}
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Contraseña</Label>
                            <Input type="password"
                                value={this.state.password} onChange={this.handleInputChange}
                                placeholder="**********"
                                name="password"
                                required pattern=".{6,10}"
                                title="Debe contener entre 6 y 10 caracteres"></Input>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Vuelva a introducir la contraseña</Label>
                            <Input type="password"
                                value={this.state.password2} onChange={this.handleInputChange}
                                placeholder="**********"
                                name="password2"
                                required pattern=".{6,10}"
                                title="Debe contener entre 6 y 10 caracteres"></Input>
                        </FormGroup>
                    </Col>
                    {/* <Col sm={10}>
                        <FormGroup row>
                            <legend>Desea Recibir notificación?</legend>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radio1" value="1"  onChange={this.handleOptionChange} />
                                    Si
                                </Label>
                            </FormGroup>
                        </FormGroup>
                    </Col>
                    <Col sm={10}>
                        <FormGroup row>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radio1" value="0"  onChange={this.handleOptionChange} />
                                    No
                                </Label>
                            </FormGroup>
                        </FormGroup>
                    </Col> */}
                    <Button type="submit" color="primary">Registrarse</Button><br></br>o<br></br>
                    <Link to={"/Login"}><i>Iniciar Sesión</i></Link>
                    {/* <a href="./Login">Logearse</a> */}
                </Form>
            </div>
            </div>
        )
    }
}
