import React from "react";
import "/src/css/style.css";
import logoeq from "/src/img/logoeq.png";

export default class Principal extends React.Component {
    render(){
        return(
            <div className="content">
                <div className="introPrincipal">
                    <div className="principalContent">
                      <img width="250px" className="logo" src={logoeq}></img>
                      <p className="logo">Electronic Queue</p>
                    </div>
                </div>
            </div>
        )
    }
}