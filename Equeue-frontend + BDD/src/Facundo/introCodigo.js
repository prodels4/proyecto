import React from "react";
import { Button, Input, Container, Col, Form, FormGroup, Label } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import {connect} from "react-redux";

const URL = "http://localhost:3001/codigo";

 class IntroCodigo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            codigo: ""
        }

        this.introducirCodigo = this.introducirCodigo.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    introducirCodigo(e) {
        e.preventDefault();

        fetch(URL + '/cosmin/' + this.state.codigo)
            .then(datos => datos.json())
            .then(infcodigo => this.props.dispatch({
                type: 'INTRO_TICKET',
                infcodigo: infcodigo,
                codigoticket: this.state.codigo
              }))
              .then(()=>{
                  this.props.history.push("/turno")
              })
            .catch(err => console.log(err));
    }

    render() {
        return (
            <div className="content">
                <div className="login">
                    <h2>Introduzca su código</h2>
                    <Form onSubmit={this.introducirCodigo}>
                        <Input type="text"
                            name="codigo"
                            value={this.state.codigo}
                            onChange={this.handleInputChange}
                            required
                            placeholder="Introduce su Código"></Input>

                        <Button type="submit"
                            color="primary">
                            Enviar
                </Button>

                    </Form>

                </div>
            </div>
        )
    }
}
export default connect()(IntroCodigo);