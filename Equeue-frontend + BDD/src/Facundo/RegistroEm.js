import React from "react";
import { Button, Input, Container, Col, Form, FormGroup, Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Redirect } from 'react-router';

const URL = "http://localhost:3001"

export default class Registro extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            email: "",
            password: "",
            password2: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleInputChange(event) {

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

    }

    handleSubmit(e) {

        e.preventDefault();

        let usuari = {
            email: this.state.email,
            password: this.state.password,
        }
        fetch(URL + '/empresa/register', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(usuari)
        })
            .then(respuesta => respuesta.json())
            .then(respuesta => {
                if (respuesta.ok === false) {
                    throw "ERROR REGISTRANT USUARI!";
                } else {
                    console.log("OK USUARI REGISTRAT!!!");
                    this.props.history.push('/loginemp');
                }
            })
            .catch(err => console.log(err));

    }
    render() {
        return (
            <div className="content">
                <div className="login">
                    <h2 className="Titulos"> Registro de Empresa</h2>
                    <Form className="form" onSubmit={this.handleSubmit}>
                        <Col>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="email"
                                    placeholder="Introduzca el Nombre de Usuario"
                                    value={this.state.email} onChange={this.handleInputChange}
                                    name="email"
                                    required
                                    title="Su nombre debe contener entre 5 y 30 carácteres"></Input>
                            </FormGroup>
                        </Col>
                        {/* <Col>
                        <FormGroup>
                            <Label>Nombre de la Empresa</Label>
                            <Input type="email"
                                value={this.state.nameEm} onChange={this.handleInputChange}
                                placeholder="Introduzca el nombre de su empresa"
                                // required pattern="[aA-zZ]{5,}"
                                name="email"
                               title="Nombre Empresa"></Input>
                        </FormGroup>
                    </Col> */}
                        <Col>
                            {/* <FormGroup>
                            <Label>Nº de Teléfono</Label>
                            <Input type="text"
                                // value={this.state.email} onChange={this.handleInputChange}
                                placeholder="Introduzca su Número"
                                name="telefono"
                                // required
                                // pattern="[0-9]{9}"
                                ></Input>
                        </FormGroup> */}
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Contraseña</Label>
                                <Input type="password"
                                    value={this.state.password} onChange={this.handleInputChange}
                                    placeholder="**********"
                                    name="password"
                                    required pattern=".{6,10}"
                                    title="Debe contener entre 6 y 10 caracteres"></Input>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Vuelva a introducir la contraseña</Label>
                                <Input type="password"
                                    value={this.state.password2} onChange={this.handleInputChange}
                                    placeholder="**********"
                                    name="password2"
                                    required pattern=".{6,10}"
                                    title="Debe contener entre 6 y 10 caracteres"></Input>
                            </FormGroup>
                        </Col>
                        <Col sm={10}>
                            {/* <FormGroup row>
                            <legend>Desea Recibir notificación?</legend>

                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radio1" required />{' '}
                                    SMS
                                </Label>
                            </FormGroup>
                        </FormGroup>
                    </Col>
                    <Col sm={10}>
                        <FormGroup row>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="radio1" required />{' '}
                                    No quiero Notificación
                                </Label>
                            </FormGroup>
                        </FormGroup> */}
                        </Col>
                        <Button type="submit" color="primary">Registrarse</Button><br></br>o<br></br>
                        <Link to={"/Loginemp"}><i>Iniciar Sesión</i></Link>
                        {/* <a href="./Login">Logearse</a> */}
                    </Form>
                </div>
            </div>
        )
    }
}