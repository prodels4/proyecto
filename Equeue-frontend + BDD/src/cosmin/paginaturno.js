import React from "react";
import {
  Button,
  Card,
  CardTitle,
  CardText,
  Toast,
  ToastBody,
  ToastHeader,
} from "reactstrap";
import { connect } from 'react-redux';

const API = "http://localhost:3001/codigo";

class CartaTurno extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      codigo: this.props.match.params.codigo,
      idCodigo: 18,
      idLista: 2,
      posicion: this.props.match.params.idCodigo,
      espera: "",
      cola: 0,
    };

    this.tiempoPromedio = this.tiempoPromedio.bind(this);
    this.cantidadPersonas = this.cantidadPersonas.bind(this);
  }

  componentDidMount() {
    this.tiempoPromedio();
    this.cantidadPersonas();
  
  }
  tiempoPromedio() {
    const url = API + "/avg/" + this.state.idLista + "/" + this.state.idCodigo;
    fetch(url)
      .then(tiempo => { return tiempo.json() })
      .then(tiempo => {
        if (tiempo.ok) {
          console.log("tiempo", tiempo);
          this.setState({
            espera: tiempo.data.promedio
          })
        }
      })
      .catch(err => console.log(err));
  }

  cantidadPersonas() {
    console.log("zzzzzzzzz",this.props.infcodlistid,this.props.infcodid);

    const url = API + "/count/" + this.props.infcodlistid + "/" + this.props.infcodid;
    fetch(url)
      .then(cantidad => { return cantidad.json() })
      .then(cantidad => {
        if (cantidad.ok) {
          console.log("cantidad ", cantidad.data)
          this.setState({
            cola: cantidad.data[0].personas
          })
        }
      })
      .catch(err => console.log(err));
  }

  render() {

    console.log("cola actual ", this.state.cola)

    if (!this.props.infcodid || !this.state.cola){
      return <h1>Cargando...</h1>
    }
    if (!this.props.infcodlistid){
      return <h1></h1>
    }

    if (this.state.cola >= 0) {
    }
      return (
        <div>
          <Alertas cola={this.state.cola} espera={this.state.espera} />
          <Card
            body
            inverse
            style={{ backgroundColor: "#333", borderColor: "#333", width: "25%" }}
          >
            <CardTitle>
              Tu codigo es: {" "}
              <span className="badge badge-light">{this.props.codigoticket}</span>
            </CardTitle>
            <CardTitle>
              Tu posicion en la cola es:{" "}
              <span className="badge badge-light">{this.state.cola + 1}</span>
            </CardTitle>
            {/* <CardText>Te quedan:{" "}
              <span className="badge badge-light">{this.state.espera}</span> min aprox.
              </CardText> */}
            <CardText>Y tienes:
          <span className="badge badge-light">{this.state.cola}</span> persona/s por delante
          </CardText>
          </Card>
        </div>
      );
    
  }
}

const Alertas = props => {
  const cola = props.cola;
  const espera = props.espera;
  const codigo = props.codigo;
  if ((cola <= 5 && cola >= 4) || (espera >= 11 && espera <= 15)) {
    return (
      <div className="p-3 my-2 rounded bg-docs-transparent-grid">
        <Toast>
          <ToastHeader className="bg-warning">Alerta de Colas!</ToastHeader>
          <ToastBody>
            Cuidado, le quedan {cola} persona/s por delante!
          </ToastBody>
          {/* <ToastBody>Le tocara su turno en {espera} min!</ToastBody> */}
        </Toast>
      </div>
    );
  } else if ((cola <= 3 && cola >= 1) || (espera >= 1 && espera <= 10)) {
    return (
      <div className="p-3 my-2 rounded bg-docs-transparent-grid">
        <Toast>
          <ToastHeader className="bg-danger">Alerta de Colas!</ToastHeader>
          <ToastBody>
            Cuidado, le quedan {cola} persona/s por delante!
          </ToastBody>
          {/* <ToastBody>Le tocara su turno en {espera} min!</ToastBody> */}
        </Toast>
      </div>
    );
  } else if (cola === "0") {
    return (
      <div className="p-3 my-2 rounded bg-docs-transparent-grid">
        <Toast>
          <ToastHeader className="bg-success">Alerta de Colas!</ToastHeader>
          <ToastBody>
            Es tu turno!! Corre insensato!
            <Button close />
          </ToastBody>
        </Toast>
      </div>
    );
  } else {
    return <></>;
  }
};
const mapStateToProps = (state) => {


  return {
      infcodid: state.codid,
      infcodlistid: state.codlistid,
      codigoticket: state.codigoticket
  }
}
export default connect(mapStateToProps)(CartaTurno);
