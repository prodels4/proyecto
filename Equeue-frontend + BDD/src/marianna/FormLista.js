import React from "react";
import "/src/css/stylesEmpresa.css";
import { Button, Form, FormGroup, Label, Input, Modal, ModalBody, ModalHeader } from 'reactstrap';

const URL = "http://localhost:3001/lista";

export default class BotonAgregarLista extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            nombre: "",
            numMax: 50,
            // durPausa: 10
        };

        this.toggle = this.toggle.bind(this);
        this.cambiaNombre = this.cambiaNombre.bind(this);
        this.cambiaNumMax = this.cambiaNumMax.bind(this);
        this.enviarForm = this.enviarForm.bind(this);
        // this.cambiaDurPausa = this.cambiaDurPausa.bind(this);
    }

    cambiaNumMax(event) {
        let numeroMaximo = event.target.value;
        if (numeroMaximo !== "") {
            numeroMaximo = parseInt(numeroMaximo);
            this.setState({
                 numMax: numeroMaximo
            })
        }

    }

    enviarForm(event) {
        event.preventDefault();

        let lista = {
            //id: this.state.id,
            ListName: this.state.nombre,
            //max_codes: this.state.numMax,
        }

        fetch(URL, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(lista)
        })
            .then(respuesta => respuesta.json())
            .then(respuesta => {
                if (respuesta.ok === false) {
                    throw "ERROR: no se pudo agregar la lista"
                } else {
                    console.log("Se pudo crear la lista");
                    this.props.updateLista();
                    this.setState({
                        id: 0,
                        nombre: "",
                        numMax: 50
                    })
                }
            })
            .catch(error => console.log(error));


        this.setState(prevState => ({
            modal: !prevState.modal
        }));

        console.log("form nombre: " + this.state.nombre.toString() + " \nnumMax: " + this.state.numMax )
    }

    cambiaNombre(event) {
        let nombreLista = event.target.value;
        
            this.setState({
                nombre: nombreLista
            })
        
    }

    // cambiaDurPausa(event) {
    //     let duracionPausa = event.target.value;
    //     if(duracionPausa !== 0 || duracionPausa !== ""){
    //         this.setState({
    //             durPausa: duracionPausa
    //         })
    //     }
    // }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    render() {
        const externalCloseBtn = <button className="close" style={{ position: 'absolute', top: '15px', right: '15px' }} onClick={this.toggle}>&times;</button>;
        return (
            <>
                <Button color="primary" onClick={this.toggle}>Agregar Lista</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} external={externalCloseBtn}jfs>

                    <ModalHeader>Agregar Lista</ModalHeader>
                    <ModalBody>

                        <Form onSubmit={this.enviarForm} >
                            <FormGroup>
                                <Label for="tituloLista">Nombre de la Lista</Label>
                                <Input required type="text" value={this.state.nombre} onChange={this.cambiaNombre} />
                            </FormGroup>
                            {/* <FormGroup>
                                <Label>Numero Máximo de Códigos en Lista</Label>
                                <Input min={5} max={1000} type="number" step="1" value={this.state.numMax} onChange={this.cambiaNumMax} />
                            </FormGroup> */}
                            {/* <FormGroup>
                                <Label>Duración de la Pausa en minutos</Label>
                                <Input min={5}  max={45} type="range" step="1" value={this.state.durPausa} onChange={this.cambiaDurPausa} />
                                <span>{this.state.durPausa}</span>
                            </FormGroup> */}
                            <Button type="submit" color="primary" >Agregar</Button>
                        </Form>

                    </ModalBody>
                </Modal>

            </>

        );
    }
}