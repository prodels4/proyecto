import React from "react";
import "/src/css/stylesEmpresa.css";
import Cajas from "./Cajas";
import { CardColumns } from 'reactstrap';
import FormLista from "./FormLista";
import { connect } from 'react-redux';

const URL = "http://localhost:3001/lista";

class PaginaEmpresa extends React.Component {
    constructor(props) {
        super(props);

        // this.state={
        //     cajas: [],
        //     index: 0
        // }

        // this.borrarCaja = this.borrarCaja.bind(this);
        // this.nuevaCaja = this.nuevaCaja.bind(this);
        // this.loadData = this.loadData.bind(this);
        this.updateLista = this.updateLista.bind(this);
    }

    updateLista() {
        fetch(URL)
            .then(datos => datos.json())
            .then(listas => this.props.dispatch({
                type: 'NUEVA_LISTA',
                listas: listas
            }))
            .catch(err => console.log(err));
    }

    componentDidMount() {
        this.updateLista();
    }

    // loadData(){
    //     fetch(URL)
    //         .then(resp  => resp.json())
    //         .then(datos => this.setState({cajas: datos}))
    //         .catch(err => console.log(err));
    // }

    // borrarCaja(caja){
    //     let nuevaCaja;

    //     fetch(URL+'/'+caja.id, {method: 'DELETE'})
    //         .then(respuesta => {respuesta.json(); console.log(respuesta)})
    //         .then(respuesta => {
    //             if(respuesta.ok === false){
    //                 throw 'ERROR: no se pudo eliminar la lista'
    //             }else{
    //                 nuevaCaja = this.state.cajas.filter(el => el.id !== caja.id);
    //                 this.setState({
    //                     cajas: nuevaCaja
    //                 })
    //             }
    //         })
    //         .catch(error => console.log(error));

    // }

    // nuevaCaja(id,nombre,numMax){
    //    fetch
    //     this.setState({
    //         cajas: [...this.state.cajas, {id:id, nombre: nombre, numMax: numMax}],
    //         index: id
    //     })
    //     console.log(this.state.cajas)
    //     console.log("index: "+this.state-index)
    // }


    render() {


        return (

            <div className="inicial">

                {this.props.admin ?
                    <FormLista updateLista={this.updateLista} />
                    :
                    <div />
                }
                
                <CardColumns>
                    <Cajas updateLista={this.updateLista} />
                </CardColumns>


            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        admin: state.admin
    }
}

export default connect(mapStateToProps)(PaginaEmpresa);