import React from "react";
import "/src/css/stylesEmpresa.css";
import { Card, CardText, CardHeader, CardTitle, Button, Spinner } from 'reactstrap';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

const URL = "http://localhost:3001/lista";

class Cajas extends React.Component {

    constructor(props) {
        super(props)

        this.deleteList = this.deleteList.bind(this);
        this.storeId = this.storeId.bind(this);
    }

    storeId(itemId, itemName) {
        this.props.dispatch({
            type: 'INTRO_IDLISTA',
            idlista: itemId,
            nombrelista: itemName
        })
    }
    deleteList(itemId) {
        fetch(URL + '/' + itemId, { method: 'DELETE' })
            .then(response => response.json())
            .then(this.props.updateLista)
            .catch(err => console.log(err))
    }
    render() {
        let lista;
        let titulo;
        let spinner = "";

        if (!this.props.listas.data) {
            titulo = "Cargando datos";
            spinner = <Spinner color="primary" />;
            return spinner;
        } else {
            let i = 0;
            lista = this.props.listas.data.map(c =>
                // <ItemCards key={c.id} id={c.id} nombre={c.nombre} numMax={c.numMax} />)
                <Card key={i++} body inverse style={{ backgroundColor: 'darkblue', borderColor: 'white' }}>
                    <CardTitle>{c.ListName}</CardTitle>
                    {this.props.admin ?
                        <Link to={"/opcioneslista/"} onClick={() => this.storeId(c.id, c.ListName)} className="btn close">Entrar</Link>
                        :
                        <Link to={"/crearticketcl/"} onClick={() => this.storeId(c.id, c.ListName)} className="btn close">Entrar</Link>
                    }
                    <br />
                    {this.props.admin ?
                        <Button close onClick={() => this.deleteList(c.id)} >Borrar</Button>
                        :
                        <div />
                    }


                </Card>)

        }


        return (
            <>
                {lista}
            </>
        );
    }
}

// class ItemCards extends React.Component {
//     constructor(props) {
//         super(props)

//         this.deleteList = this.deleteList.bind(this);
//     }

//     deleteList(itemId) {
//         fetch(URL + '/' + itemId, { method: 'DELETE' })
//             .then(response => response.json())
//             .then(this.props.updateLista)
//             .catch(err => console.log(err))
//     }

//     render() {
//         return (
//             <Card body inverse style={{ backgroundColor: 'darkblue', borderColor: 'white' }} >

//                 <CardHeader>
//                     {this.props.nombre}

//                     {this.props.admin ?
//                         <Button close onClick={() => this.deleteList(this.props.id)} />
//                         :
//                         <div />
//                     }
//                     {this.props.admin ?
//                         <Link to={"/opcioneslista/" + this.props.id} className="btn close">></Link>
//                         :
//                         <Link to={"/crerarticketcl/" + this.props.id} className="btn close">></Link>
//                     }

//                 </CardHeader>
//                 <CardText>Numero Maximo de Codigos: {this.props.numMax}</CardText>
//                 {/* <CardText>Duración de la Lista en Pausa: {this.props.durPausa} mins</CardText> */}
//             </Card>
//         );
//     }
// }
const mapStateToProps = (state) => {
    return {
        listas: JSON.parse(JSON.stringify(state.listas)),
        admin: state.admin
    }
}

export default connect(mapStateToProps)(Cajas);