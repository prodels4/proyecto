
// El Redux contiene:
// - La lista de "codigos" (o tickets) de la base de datos.
// - Email del usuario logueado
// - Si el usuaio logueado es Admin o no
// - id del usuario logueado
// - Informacion a partir de un codigo introducido
// - Id de la lista seleccionada

const appReducer = (state = {
    codigos: [],
    email: "",
    admin: "",
    listas: [],
    id: "",
    codid: "",
    codlistid: "",
    idlista: "",
    nombrelista: "",
    infcodigo: [],
    codigoticket: ""
}, action) => {

    let newState = JSON.parse(JSON.stringify(state));

    switch (action.type) {

        case 'NOU_ELEMENT':

        newState.codigos = JSON.parse(JSON.stringify(action.codigos));
        
        return newState;

        case 'SET_USUARIO':

        newState.id = action.id;
        newState.email = action.email;
        newState.admin = action.admin;

        console.log(newState.email)

        return newState;

        case 'NUEVA_LISTA':
        newState.listas = JSON.parse(JSON.stringify(action.listas));

        return newState;

        case 'INTRO_TICKET':


        // newState.codid = action.infcodigo.data.id
        // newState.codlistid = action.infcodigo.data.idlista
        newState.infcodigo = JSON.parse(JSON.stringify(action.infcodigo.data));
        newState.codigoticket = action.codigoticket
        newState.codid = newState.infcodigo.id
        newState.codlistid = newState.infcodigo.idList



        return newState;

        case 'INTRO_IDLISTA':

        newState.idlista = action.idlista;
        newState.nombrelista = action.nombrelista;

        

        return newState;

        default:
            return state;
    }

}
export default appReducer;