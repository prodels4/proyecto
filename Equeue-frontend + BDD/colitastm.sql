CREATE DATABASE  IF NOT EXISTS `colitastm` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `colitastm`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: colitastm
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `active_notification` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (2,'testerClient2','testerClient@email.com','testerClient2',0),(3,'testerClient3','testerCliente3@email.com','testerClient3',0),(5,'tester','tester@tester.com','$2a$10$OCAYyfZ4NyRNyrnJAKJbHO/sQFcEd5KXazlKPY/x0unFFihpztLy6',0),(6,'','usu@usu.com','$2a$10$9kc0WNg.DelfNek76h8o7uMmqPe0WjN/WHQ8uL87Ds5N4jksQfsgS',0),(7,'','usuario@usuario','$2a$10$2AjFm66BNwfO0lWWzQbdWe1TYmUjSIJDtG4SXm0g91dezHhMouME6',NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigos`
--

DROP TABLE IF EXISTS `codigos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `idClient` int(11) DEFAULT NULL,
  `idList` int(11) DEFAULT NULL,
  `is_closed` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `Codigo_UNIQUE` (`code`),
  KEY `fk_Codigos_Clientes1_idx` (`idClient`),
  KEY `fk_Codigos_Listas1_idx` (`idList`),
  CONSTRAINT `fk_Codigos_Clientes1` FOREIGN KEY (`idClient`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Codigos_Listas1` FOREIGN KEY (`idList`) REFERENCES `listas` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigos`
--

LOCK TABLES `codigos` WRITE;
/*!40000 ALTER TABLE `codigos` DISABLE KEYS */;
INSERT INTO `codigos` VALUES (10,'k978hdjka',NULL,NULL,0,'2019-09-23 07:29:07','2019-09-23 07:29:07'),(11,'k0w3bkwz',NULL,NULL,1,'2019-09-23 07:30:06','2019-09-23 07:30:06'),(25,'k11u2yop',NULL,13,1,'2019-09-27 07:58:22','2019-09-27 07:58:22'),(26,'k11u300b',NULL,13,1,'2019-09-27 07:58:23','2019-09-27 07:58:23'),(27,'k11u31a3',NULL,13,0,'2019-09-27 07:57:41','2019-09-27 07:57:41'),(28,'k11u32mj',NULL,13,1,'2019-09-27 08:19:43','2019-09-27 08:19:43'),(29,'k11u348z',NULL,13,1,'2019-09-27 08:19:43','2019-09-27 08:19:43'),(30,'k11u7k1m',NULL,13,1,'2019-09-27 08:19:44','2019-09-27 08:19:44'),(31,'k11uv2aj',NULL,14,0,'2019-09-27 08:19:29','2019-09-27 08:19:29'),(32,'k11uv3pu',NULL,14,0,'2019-09-27 08:19:31','2019-09-27 08:19:31'),(33,'k11uv52p',NULL,14,0,'2019-09-27 08:19:32','2019-09-27 08:19:32'),(34,'k11uv6a2',NULL,14,0,'2019-09-27 08:19:34','2019-09-27 08:19:34'),(35,'k11uv7de',NULL,14,0,'2019-09-27 08:19:36','2019-09-27 08:19:36'),(36,'k11uv8sq',NULL,14,0,'2019-09-27 08:19:37','2019-09-27 08:19:37');
/*!40000 ALTER TABLE `codigos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `idList` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`email`),
  KEY `fk_Usuarios_Listas_idx` (`idList`),
  CONSTRAINT `fk_Usuarios_Listas` FOREIGN KEY (`idList`) REFERENCES `listas` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
INSERT INTO `empleados` VALUES (7,'empleadoNoAdmin','empAdmin@emp.com','$2a$10$uuEdYwdLfpZ7cDSiVAPSDug4jTzJENruPrNK9HdSz7cMw7zXo.fci',0,NULL),(8,'','admin@admin.com','$2a$10$pG2uxSQdeNtUaqSsqEmc7.3S8McKszAV/CC9BSEXKuSLfVycQShf2',0,NULL),(9,'','admin@admin','$2a$10$/zpakIMqq3zZPNtZuNijI.6cxHX3O/s5YsQGelWNHg4fs.1AuwMCi',0,NULL);
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listas`
--

DROP TABLE IF EXISTS `listas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ListName` varchar(50) NOT NULL,
  `max_codes` int(45) DEFAULT 50,
  `duration_pause` int(11) DEFAULT NULL,
  `is_paused` tinyint(1) DEFAULT 1,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `NombreLista_UNIQUE` (`ListName`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listas`
--

LOCK TABLES `listas` WRITE;
/*!40000 ALTER TABLE `listas` DISABLE KEYS */;
INSERT INTO `listas` VALUES (13,'admin',50,NULL,1,'2019-09-27 07:57:31','2019-09-27 07:57:31'),(14,'Fiesta Formacion JS Mataro',50,NULL,1,'2019-09-27 08:19:24','2019-09-27 08:19:24'),(15,'Prueba',50,NULL,1,'2019-09-27 08:19:59','2019-09-27 08:19:59');
/*!40000 ALTER TABLE `listas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-27 11:39:19
