const express = require('express');
// const cors = require('cors');
const app = express();

app.use(express.json());
// app.use(cors());



const clientRouter = require('./routes/clientes-controller');
const codRouter = require('./routes/codigo-controller');
const empRouter = require('./routes/empleado-controller');
const listRouter = require('./routes/lista-controller');

app.use('/cliente', clientRouter);
app.use('/codigo', codRouter);
app.use('/empresa', empRouter);
app.use('/lista', listRouter);

const port = 3001;
app.listen(port, () => console.log('open on http://localhost:' + port));



const indexRoute = require('./routes/index-controller');//just for testing

app.use('/', indexRoute);//just for testing

// app.use(express.static('front'));