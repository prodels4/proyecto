module.exports = (sequelize, DataTypes) => {
    const Codigo = sequelize.define('Codigo', {
        code:{
            type: DataTypes.STRING,
            unique: true
        },
        idClient: {
            type: DataTypes.INTEGER,
            references:{
                model: './Cliente.js',
                key: 'id'
            }
        },
        idList: {
            type: DataTypes.INTEGER,
            references:{
                model: './Listas.js',
                key: 'id'
            }
        },
        is_closed: DataTypes.BOOLEAN
    }, {tableName: 'codigos', timestamps: true});

    return Codigo;
};