module.exports = (sequelize, DataTypes) => {
    const Token = sequelize.define('Token', {
  
      token: DataTypes.STRING,
      hores: DataTypes.INTEGER,
      email: DataTypes.STRING,
      
    }, { tableName: 'tokens'});
    
    return Token;
  };