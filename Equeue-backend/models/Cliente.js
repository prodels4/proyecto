module.exports = (sequelize, DataTypes) => {
    const Cliente = sequelize.define('Cliente', {
        // id: {
        //     type: DataTypes.INTEGER,
        //     primaryKey: true,
        //     autoIncrement: true
        // },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: DataTypes.STRING,
        active_notification: DataTypes.BOOLEAN
    }, { tableName: 'clientes', timestamps: false});

    return Cliente;
}