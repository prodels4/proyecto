module.exports = (sequelize, DataTypes) => {
    const Empleados = sequelize.define('Empleados', {
        name_lastname: DataTypes.STRING,
        email:{
            type: DataTypes.STRING,
            unique: true
        },
        password: DataTypes.STRING,
        is_admin: DataTypes.BOOLEAN,
        idList:{
            type: DataTypes.INTEGER,
            references:{
                model: './Listas.js',
                key: 'id'
            }
        }
    }, {tableName: 'empleados', timestamps: false});

    return Empleados;
};