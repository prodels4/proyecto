module.exports = (sequelize,DataTypes) => {
    const Listas = sequelize.define('Listas', {
        ListName: {
            type: DataTypes.STRING,
            unique: true
        },
        max_codes: DataTypes.INTEGER,
        duration_pause: DataTypes.INTEGER,
        is_paused: DataTypes.BOOLEAN

    }, {tableName: 'listas', timestamps: true});
    
    return Listas;
};