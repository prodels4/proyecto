const express = require('express');
const router = express.Router();

const modelo = require('../models/index.js');


router.all('*', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
  })
  
  router.all('/:algo', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
  })

/*find all codes*/
router.get('/lista', (req,res, next)=>{
    modelo.Codigo.findAll()
        .then(el => res.json({ok:true, data:el}))
        .catch(err => res.json({ok:false, error:err}));
});

/*finds one code by the id*/
router.get('/:id', (req, res) => {
    let idLista = req.params.id;

    modelo.Codigo.findAll({ where: {idList: idLista, is_closed: false} })
        .then(el => res.json({ok:true, data:el}))
        .catch(err => res.json({ok:false, error:err}));
})

/*finds all codes only if is_closed is false*/ /* Danilo's part */ //missing adding teh idList too
router.get('/', (req, res) => {

    modelo.Codigo.findAll({ where: {is_closed: false} })
        .then(el => res.json({ok:true, data:el}))
        .catch(err => res.json({ok:false, error:err}));
});

/*Calculate the average of all the codes*/ /* Cosmin's Part */
router.get('/avg/:lista/:id', (req,res,next) => {

    let idLista = req.params.lista;
    let idCode = req.params.id;

    modelo.sequelize.query('SELECT ROUND(AVG(TIMESTAMPDIFF(MINUTE,createdAt,updatedAt)), 0) as `promedio` FROM `codigos` where id < '+idCode+' AND is_closed = true AND idList = '+idLista, { type: modelo.sequelize.QueryTypes.SELECT})
        .then(el => res.json({ok:true, data:el[0]}))
        .catch(err => res.json({ ok:false, error: err}));
});

/* send the cuantity of people that is */
router.get('/count/:lista/:id', (req,res) => {
    let idLista = req.params.lista;
    let idCode = req.params.id;

    modelo.sequelize.query('select COUNT(id) as `personas` from `codigos` where id < '+idCode+' AND is_closed = false AND idList = '+idLista, { type: modelo.sequelize.QueryTypes.SELECT})
        .then(el => res.json({ok:true, data:el}))
        .catch(err => res.json({ ok:false, error: err}));
})

/* busca por codigo y muestra el id del codigo y el idList*/
router.get('/cosmin/:codigo', (req,res)=>{
    let codeName = req.params.codigo;

    modelo.Codigo.findOne({attributes: ['id', 'idList'], where: {code: codeName}})
        .then(el => res.json({ok: true, data:el}))
        .catch(err => res.json({ok:false, error:err}));
})

/*Create a new code*/
router.post('/', (req,res)=>{
    modelo.Codigo.create(req.body)
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:err }));
});

/*Update a code by the id*/
router.put('/:id', (req,res) => {
    let idCode = req.params.id;

    modelo.Codigo.findOne({where: {id: idCode}})
        .then(el => el.update(req.body))
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:err }));
});

/*Deletes 1 code from teh database by the id*/
router.delete('/:id', (req,res) => {
    let idCode = req.params.id;

    modelo.Codigo.destroy({where: {id: idCode}})
        .then(el => res.json({ok: true, data:el}))
        .catch(err => res.json({ok:false, error:err}));
});

module.exports = router;