const express = require('express');
const router = express.Router();

const modelo = require('../models/index.js');

router.all('/', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

router.all('/:algo', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

/*Find all Lists*/
router.get('/',(req,res,next) => {
    modelo.Listas.findAll()
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:err }));
});

/* Find one list by the id */
router.get('/:id', (req, res, next) => {
    let idList = req.params.id;

    modelo.Listas.findOne({ where: {id: idList} })
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:err }));
});

/*Find a list by its name*/
router.get('/name/:name', (req,res,next) => {
    let nameList = req.params.name;

    modelo.Listas.findOne({ where: {ListName: nameList} })
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:er }));
});

/*Insert into list*/
router.post('/', (req, res, next) =>{
    modelo.Listas.create(req.body)
    .then(el => res.json({ ok:true, data:el }))
    .catch(err => res.json({ ok: false, data:err }));
});

/*Updates a list by its id*/
router.put('/:id', (req,res,next) => {
    let idList = req.params.id;

    modelo.Listas.findOne({ where: {id: idList} })
        .then(el => el.update(req.body))
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:err }));

});

/*Delete a list by its id */
router.delete('/:id', (req,res,next) => {
    let idList = req.params.id;

    modelo.Listas.destroy({ where: {id: idList} })
        .then(el => res.json({ ok:true, data:el }))
        .catch(err => res.json({ ok:false, error:err }));

});

module.exports = router;