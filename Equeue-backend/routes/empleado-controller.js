const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

const modelo = require('../models/index.js');
const JWT = require('jsonwebtoken');
const SECRET = "merdolino";


router.all('/', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

router.all('/:algo', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

/*Find all*/
router.get('/', (req, res, next) => {
    modelo.Empleados.findAll()
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/*Find one emp by the id*/
router.get('/:id', (req, res, next) => {
    let idEmp = req.params.id;

    modelo.Empleados.findOne({ where: { id: idEmp } })
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/* Finds one emp by the username */
router.get('/username/:usernm', (req, res, next) => {
    let usernameEmp = req.params.usernm;

    modelo.Empleados.findOne({ where: { username: usernameEmp } })
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/* Finds only the admin emp */
router.get('/admin/:admin', (req, res, next) => {
    let isAdmin = req.params.admin;

    modelo.Empleados.findAll({ where: { is_admin: isAdmin } })
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/*create a emp*/
router.post('/register', (req, res, next) => {
    const hash = bcrypt.hashSync(req.body.password, 10);
    req.body.password = hash;

    modelo.Empleados.create(req.body)
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/* LOGIN */
router.post('/login', (req, res, next) => {

    const { email, password } = req.body;

    if (!email || !password) {
        return res.status(400).json({ ok: false, error: 'email o password no recibidos' });
    }

    modelo.Empleados.findOne({ where: { email } })
        .then(empleado => {
            if (bcrypt.compareSync(password, empleado.password)) {
                return empleado;
            } else {
                throw "email o password incorrectos";
            }
        })
        .then(empleado => {
            const payload = { id: empleado.id, email: empleado.email };
            const token = JWT.sign(payload, SECRET, {
                expiresIn: '1h'
            });
            res.status(200).cookie('token', token, { httpOnly: true }).json({ id: empleado.id, email: empleado.email, admin: empleado.is_admin });
        })
        .catch((err) => res.status(401).json({ ok: false, error: err }));
});

/*update a emp if only the user is admin*/
router.put('/admin/:id', (req, res, next) => {
    let idEmp = req.params.id;

    modelo.Empleados.findOne({ where: { id: idEmp, is_admin: true } })
        .then(el => el.update(req.body))
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/*update a emp if only the user is not an admin*/
router.put('/:id', (req, res, next) => {
    let idEmp = req.params.id;

    modelo.Empleados.findOne({ where: { id: idEmp } })
        .then(el => el.update(req.body))
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

/*deletes an emp*/
router.delete('/:id', (req, res) => {
    let idEmp = req.params.id;

    modelo.Empleados.destroy({ where: { id: idEmp } })
        .then(el => res.json({ ok: true, data: el }))
        .catch(err => res.json({ ok: false, error: err }));
});

module.exports = router;