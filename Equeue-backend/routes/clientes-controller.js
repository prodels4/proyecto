const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const modelo = require('../models/index.js');
const withAuth = require('../middleware');


const JWT = require('jsonwebtoken');
const SECRET = "merdolino";



router.all('/', (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
  res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
})

router.all('/:algo', (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
  res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
})

router.get('/check', withAuth, (req, res) => {
  res.sendStatus(200);
});


/* POST LOGIN */
router.post('/login', (req, res) => {

  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({ ok: false, error: "email o password no recibidos" });
  }

  modelo.Cliente.findOne({ where: { email } })
    .then(usuari => {

      if (bcrypt.compareSync(password, usuari.password)) {
        return usuari;
      } else {
        throw "email o password incorrectos";
      }
    })
    .then(usuari => {

      const payload = { id: usuari.id, email: usuari.email };
      const token = JWT.sign(payload, SECRET, {
        expiresIn: '1h'
      });
      res.status(200).cookie('loginCliente', token, { httpOnly: true }).json({ id: usuari.id ,email: usuari.email });

    })
    .catch((error) => res.status(401).json({ ok: false, error: error }));

});

/*find all clients*/
router.get('/', (req, res, next) => {
  modelo.Cliente.findAll()
    .then(el => res.json({ ok: true, data: el }))
    .catch(err => res.json({ ok: false, error: err }))
})

/*find One client by the id*/
router.get('/:id', (req, res, next) => {
  let idClient = req.params.id;

  modelo.Cliente.findOne({ where: { id: idClient } })
    .then(el => res.json({ ok: true, data: el }))
    .catch(err => res.json({ ok: false, error: err }))
})

/*find One client by the phone*/
router.get('/phone/:phone', (req, res, next) => {
  let phoneClient = req.params.phone;

  modelo.Cliente.findOne({ where: { phone: phoneClient } })
    .then(el => res.json({ ok: true, data: el }))
    .catch(err => res.json({ ok: false, error: err }));
});

/*Insert client */  /* register client */
router.post('/register', (req, res, next) => {
  const hash = bcrypt.hashSync(req.body.password, 10);
  req.body.password = hash;

  modelo.Cliente.create(req.body)
    .then(el => res.json({ ok: true, data: el }))
    .catch(err => res.json({ ok: false, error: err }));
});

/*Updates a client by the id*/
router.put('/:id', (req, res, next) => {
  let idClient = req.params.id;

  modelo.Cliente.findOne({ where: { id: idClient } })
    .then(el => el.update(req.body))
    .then(el => res.json({ ok: true, data: el }))
    .catch(err => res.json({ ok: false, error: err }))
});

/* Deletes a client by a id */
router.delete('/:id', (req, res, next) => {
  let idClient = req.params.id;

  modelo.Cliente.destroy({ where: { id: idClient } })
    .then(el => res.json({ ok: true, data: el }))
    .catch(err => res.json({ ok: false, error: err }))
});

module.exports = router;